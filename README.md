# PPP

### Version
1.0.0 - End of project

### Tech

PPP uses a number of open source projects to work properly:

* [node.js] - For make a web 2.0 you must to use it
* [Express] - For improve performance of node.js

### Installation

- You need Node.js installed :
  + You can find it here :
  > https://nodejs.org/download/
  + For debian user's :
  ```sh
  sudo apt-get install nodejs-legacy
  sudo apt-get install npm
  ```
  + And for Os X User's (with brew installed) :
  ```sh
  brew install nodejs
  brew install npm
  ```

After : 
```sh
$ git clone https://github.com/bsulyan/PPP
$ cd PPP
$ npm install
$ sudo -s
$ node /bin/www 
```

And access to http://localhost:8112/

### Todo's

 - Nothing

### Already Set Features

 - All
