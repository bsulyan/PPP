var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Index' });
});

/* GET fiche1 page. */
router.get('/system', function(req, res) {
  res.render('fiche1', { title: 'Ingénieur en Système embarqué' });
});

/* GET fiche2 page. */
router.get('/network', function(req, res) {
  res.render('fiche2', { title: 'Ingénieur Réseau' });
});

/* GET about page. */
router.get('/about', function(req, res) {
  res.render('about', { title: 'À Propos' });
});

module.exports = router;
