Présentation PPP : Ingénieur réseau
 
 
Introduction
 
De nos jours, les objets connectés sont de plus en plus présent, gps, montre, téléphone, l'informatique est partout. Il nous est donc venu l'idée de se pencher un peu plus sur la question de ces objets qui font maintenant notre quotidien. Les secteurs d'activités y sont donc également nombreux, que ce soit dans le monde de la finance ou bien dans l'automobile, de plus en plus d'entreprise recherche du personnel pour développer ce marché.
Après une recherche concernant les formations à suivre pour accéder au métier d'ingénieur en système embarqué, nous avons constaté que cela allait de paire avec le réseau. En effet, si les objets connectés ne sont pas reliés entre eux, ils perdent en efficacité. Nous nous sommes alors penché sur ce métier qui est ingénieur en réseau. 
Curieux, nous nous sommes donc intéressé à ce métier que nous ne connaissions pas. 

 
 
Matériel & Méthodes
 
Les documents utilisés proviennent d’Internet ainsi que d’entretiens ou discussions avec des étudiants actuellement en Master Réseaux.
Nous avons réalisé divers entretiens :
-         Un premier, le 5 avril Julien Montavont qui est le responsable du M2 RISE
-         Puis avec Maxime Cogney (aka Café Dazzlehoff) qui est un étudiant diplomé du Master RISE      

 
 
Résultats 
 
Depuis quelques années les objets embarqués sont omniprésents, que ce soit dans les voitures, dans les équipements électroménager ou encore à l'arrêt de bus. De plus en plus présente, ces machines n'ont pas les mêmes contraintes ni les mêmes usages. Certaines demandent une certaines puissance de calcul, d'autres une certaine autonomie. Ceci est donc le rôle de l'ingénieur en système embarqué de prendre tout ça en compte en proposant des équipements ayant les meilleurs performances pour un usage donné. Curieux, celui ci devra chercher ce qu'il se fait de mieux pour son projet, que ce soit en logiciel comme en matériel. Ceci afin de pouvoir donner à l'utilisateur le plus de confort possible. 

Malheureusement celui ci ne peux pas tout faire lui même et devra s'appuyer sur les recherches d'autres professionels, notamment sur l'ingénieur en réseau, sans lequel la communication entre les équipements ne seraient pas possible. Que ce soit avec ou sans fil, les objectifs sont toujours les mêmes, avoir un débit maximale ainsi qu'une perte de signal minimale. Il faudra encore ajouter à tout cela des protocoles de sécurités, ainsi que des adressages ip en nombre suffisant. 
 
 
 
Discussion

Ce qui est surprenant c'est que l'ingénieur en système embarqué ne fait pas que du logiciel, il doit être à l'aise aussi bien en hardware qu'en software. Par contre nous savions qu'il avait beaucoup de contraintes et que celle ci pouvaient changer en fonction du projet. Ce métier est difficile car il demande une grande connaisance et expérience en objet embarqué.
L'ingénieur en réseau a plus de contrainte en sécurité, car si il ne fait pas assez attention aux protocoles qu'il utilise, cela peut retomber assez vite sur les projets utilisant ces protocoles car il y a un risque qu'on puisse y trouver une faille permettant un accès à la machine ou bien l'interception d'informations confidentielles. 
 
Conclusion

Nous avons pu constater à l'issue de nos recherches, que ces deux métiers sont importants car omniprésents dans le monde actuelle. En effet, tout le monde utilise ces nouvelles technlogies, que ce soit dans votre voiture ou sur votre téléphone, il y une part de système embarqué (l'interface homme machine) ainsi qu'une part de réseau (les communications gps, ou le réseau téléphonique). 
Être ingénieur réseau ne veux pas dire qu'il suffit de maintenir une connexion déjà existante entre deux machines, mais qu'il faut créer de nouveaux protocoles, ainsi que de nouvelles technologies de réseau pour pouvoir avoir de meilleur débit, ainsi que de meilleur performance sur les appareils existants et à venir tout en gardant une part de connection standard pour continuer à fonctionner avec les équipements actuelles. 
Pareillement, être ingénieur en système embarqué ne veux pas dire faire du code pour des hardwares déjà existant, son travail est aussi de s'intéresser aux nouvelles technologies afin de les intégrer à ses projets et créer des hardware spécifique à l'usage qu'il veut faire de sa machine. 

Annexes
 
Sources : 
  -  Entretiens
  -  Internet :
     -  L’étudiant
     -  https://www.prospects.ac.uk/job-profiles/network-engineer
     -  http://searchnetworking.techtarget.com/answer/What-are-job-responsibilities-of-a-network-engineer
